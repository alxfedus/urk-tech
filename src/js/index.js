import $ from 'jquery';

$(document).ready(function() {

	//JS-1
	$('.button').on('click', function(e) {
		e.preventDefault()
		const firstname = $('input[name="firstname"]').val()
		const lastname = $('input[name="lastname"]').val()
		const address = $('textarea[name="address"]').val()
		
		let person = new Person(firstname,lastname,address)
		$('.js-1').append(`<p>${person.concat()}</p>`)
	})

	
	//JS - 2
	const input1 = $('.input-1')
	const input2 = $('.input-2')
	const pattern = new RegExp('^.+@.+\..+$') //с регуляркой не заморачивался сильно

	input2.on('keyup', function() {
		if ($(this).val() === '0000') {
			input1.focus()
		}
	})

	input1.on('blur', function() {
		const value = $(this).val()
		if (pattern.test(value)) {
			alert(`Вы ввели ${value}`)
		}
	})

});

class Person {
	constructor(firstname,lastname,address) {
		this.firstname = firstname
		this.lastname = lastname
		this.address = address
	}

	concat() {
		const result = `Имя: ${this.firstname} | Фамилия: ${this.lastname} | Адрес: ${this.address}`
		return result
	}
}
